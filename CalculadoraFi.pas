Program Calculadorafibonacci;

uses crt;

var
a: integer;
b: integer;
fibonacci: integer;
n: integer;
contador: integer;
lugar: integer;
esfibonacci: boolean;
opcion: char;


Begin 
   Repeat
   clrscr;
    Writeln ('Bienvenido a mi calculadora Fibonacci, te saluda Alexaris');
      Writeln ('Ud. se encuentra en el MENU, escriba la opcion de su preferencia:');
       writeln;
       Writeln ('1. Informacion sobre la sucesion fibonacci');
       Writeln ('2. Calculo de posicion');
       Writeln ('3. Impresion de sucesion');
       Writeln ('4. Salir');
       readln (opcion);  
       clrscr; 
       
       
   case opcion of 
   
   
        '1': begin
            Writeln ('   *************************************************************************');
	            Writeln ('   *******          Informacion sobre la sucesion Fibonacci          *******');
	            Writeln ('   *************************************************************************');
	            
	            Writeln ('   FIBONACCI EN QUE CONSISTE?');
	            Writeln;
				Writeln ('      En matematicas, la sucesion o serie de fibonacci es la siguiente susecion');
				Writeln ('   infinita de numeros naturales: 1,1,2,3,5,8,13,21.. Comienza con los numeros');
				Writeln ('   1 y 1 y a partir de estos cada termino es la suma de los dos anteriores.');
				Writeln;
				Writeln ('   CUANDO SE DESCUBRIO LA SUCESION DE FIBONACCI?');
				Writeln;
				Writeln ('      Esta sucesion fue descrita en Europa por Leonardo de Pisa, matematico');
				Writeln ('   italiano del siglo XIII tambien conocido como Fibonacci.');
				Writeln ('   Fibonacci presento la sucesion en su libro Liber Abaci, publicado en 1202.');
				Writeln;
				Writeln ('   APLICACIONES EN LA ACTUALIDAD');
				Writeln;
				Writeln ('     Tiene numerosas aplicaciones en ciencias de la computacion, matematica,');
				Writeln ('   tambien aparece en configuraciones biologicas, como por ejemplo en ');
				Writeln ('   las ramas de los arboles.');
				Writeln;
				Writeln ('     Actualmente Fibonacci entra en las tecnicas de trading,  engloban ');
				writeln ('   a una serie de herramientas de analisis basadas en la secuencia  ');
				Writeln ('   y proporciones de Fibonacci que representan leyes geometricas de la ');
				Writeln ('   naturaleza y el comportamiento humano aplicadas a los mercados financieros.');
				Writeln;
			   
				Write ('    Presione ENTER para volver al menu');
				readln;
          end;
          
          
          '2':begin
              Writeln ('   *************************************************************************');
	            Writeln ('                            Calculo de Posicion                             ');
	            Writeln ('   *************************************************************************');
                Writeln;
                Writeln ('      Ingrese un numero para verificar que pertenece a la sucesion Fibonacci.');
                Writeln;
				Write   ('      Introduzca un numero: ');
				readln(n);
				
				a:=1;
				b:=1;
				lugar:=1;
				fibonacci:=b;
				esfibonacci:=true;
				
				
				while (fibonacci <= n) do
				begin
					lugar:= lugar+1;
				
					if fibonacci = n then
					    begin
						    esfibonacci:= true;
					    end
					else
					    begin
					        esfibonacci:= false;
					    end;
				
					fibonacci:=a + b;
					a:=b;
					b:=fibonacci;
				end;
				
				Writeln;
				Writeln ('      Resultado:');
				
				
				if (esfibonacci = true) then
					begin
					    if (n = 1) then
					        begin
						        Writeln ('      El numero 1 pertenece a la sucesion y se encuentara en la posicion: 1 y 2');
						    end
						else
					        begin    
					    	    Writeln ('      El numero ' ,n, ' pertenece a la sucecion y esta en la posicion: ',lugar);
					    	end;
					end
				else 
					begin
						Writeln ('      El numero ' ,n, ' no pertenece a la sucesion');
					end;
				
					Writeln;
				    Writeln ('   *************************************************************************');
					Write   ('    Presione ENTER para volver al MENU');
					Readln;
				end;

        
        
         
        '3':begin
            Writeln ('   *************************************************************************');
	            Writeln ('   *******                  Impresion de Sucesion                    *******');
	            Writeln ('   *************************************************************************');
                Writeln;
                
				Writeln ('  Ingrese el numero de posicion que desea y se mostrara el valor Fibonacci.');
				Writeln;
				Write   ('  Introduzca un numero de posicion: ');
				readln  (n);

                a:=1;
                b:=1;
                fibonacci:=b;
                
                for contador:= 2 to n-1 do
                    begin
                        fibonacci:= a + b;
                        a:= b;
                        b:= fibonacci;
                    end;
                

					Writeln;
					Writeln ('  Resultado:');
					Writeln ('  El valor Fibonacci para la posicion ',n,' es: ', fibonacci);
					Writeln;
				    Writeln ('  *************************************************************************');
					Write   ('  Presione ENTER para volver al MENU');
					Readln;
				//end;
          end;
          
         '4': begin
         Writeln ('Gracias por usar mi calculadora fibonacci, Vuelve pronto. Alexaris Luna ;)');
        writeln;
        writeln ('presione una ENTER para salir');
         readln;
         end;
         end;
     Until (opcion='4')
 
 End.
